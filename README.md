# LoadingMask Component

The `LoadingMask` component is a customizable loading overlay for Vue applications. It displays a loading message and a progress indicator (either linear or circular) while content is being loaded.

## Demo

Storybook available [here](https://metabohub.pages.mia.inra.fr/web-components/viz-mask/)


## Props

- `bgColor` (String, default: `'black'`): Background color of the loading mask.
- `fontColor` (String, default: `'white'`): Font color of the loading text.
- `fontSize` (Number, default: `64`): Font size of the loading text.
- `progressColor` (String, default: `'#04AA6D'`): Color of the progress indicator.
- `bgOpacity` (Number, default: `1`): Opacity of the background.
- `progressStyle` (String, default: `'linear'`): Style of the progress indicator. Can be either `'linear'` or `'circular'`.

## Slots

- `loading-text`: Slot for custom loading text. Default is "Loading...".
- `loading-image`: Slot for custom loading image or progress indicator. Default is a progress indicator based on `progressStyle`.

## Examples

### Default

```
<LoadingMask />
```

### Change background, font, progress bar color, font size, or background opacity

```
<LoadingMask 
	bg-color="green"
	font-color="white"
	progress-color="yellow"
	:bg-opacity="0.6"
	:font-size="12" />
```

### Select circular or linear progress bar

```
<LoadingMask progress-style="circular" />
```

### Use a slot to put anything else at the place of the text

```
<LoadingMask>
	<template v-slot:loading-text>
		<h2>This is loading...</h2>
		<h3>Please be patient.................</h3>
	</template>
</LoadingMask>
```

### Use a slot to replace the progress bar by anthything else

```
<LoadingMask>
	<template v-slot:loading-image>
		<svg xmlns="http://www.w3.org/2000/svg">
			<circle fill="#FF156D" stroke="#FF156D" stroke-width="15" r="15" cx="35"
				cy="30">
				<animate attributeName="cx" calcMode="spline" dur="2"
					values="35;165;165;35;35"
					keySplines="0 .1 .5 1;0 .1 .5 1;0 .1 .5 1;0 .1 .5 1"
					repeatCount="indefinite" begin="0"></animate>
			</circle>
			<circle fill="#FF156D" stroke="#FF156D" stroke-width="15" opacity=".8" r="15"
				cx="35" cy="30">
				<animate attributeName="cx" calcMode="spline" dur="2"
					values="35;165;165;35;35"
					keySplines="0 .1 .5 1;0 .1 .5 1;0 .1 .5 1;0 .1 .5 1"
					repeatCount="indefinite" begin="0.05"></animate>
			</circle>
			<circle fill="#FF156D" stroke="#FF156D" stroke-width="15" opacity=".6" r="15"
				cx="35" cy="30">
				<animate attributeName="cx" calcMode="spline" dur="2"
					values="35;165;165;35;35"
					keySplines="0 .1 .5 1;0 .1 .5 1;0 .1 .5 1;0 .1 .5 1"
					repeatCount="indefinite" begin=".1"></animate>
			</circle>
			<circle fill="#FF156D" stroke="#FF156D" stroke-width="15" opacity=".4" r="15"
				cx="35" cy="30">
				<animate attributeName="cx" calcMode="spline" dur="2"
					values="35;165;165;35;35"
					keySplines="0 .1 .5 1;0 .1 .5 1;0 .1 .5 1;0 .1 .5 1"
					repeatCount="indefinite" begin=".15"></animate>
			</circle>
			<circle fill="#FF156D" stroke="#FF156D" stroke-width="15" opacity=".2" r="15"
				cx="35" cy="30">
				<animate attributeName="cx" calcMode="spline" dur="2"
					values="35;165;165;35;35"
					keySplines="0 .1 .5 1;0 .1 .5 1;0 .1 .5 1;0 .1 .5 1"
					repeatCount="indefinite" begin=".2"></animate>
			</circle>
		</svg>
	</template>
</Loading-mask>
```

## Contents

This component includes :

- Vue 3
- [Vuetify 3](https://vuetifyjs.com/en/components/all/)
- Typescript
- ESlint
- npm packaging configuration
- [vitest](https://vitest.dev/guide/)
- [storybook](https://storybook.js.org/docs/vue/get-started/whats-a-story)

## Files to modify for the npm packaging

This template is configured to build and publish an npm package on GitLab, following this [tutorial](https://forgemia.inra.fr/metabohub/mth/-/wikis/mth2-wp5-t3/webcomponents-npm). 

Replace 'ComponentExample' with the name of the component to publish in these files :
- lib/main.js
- package.json
- vite.config.ts

The package will be published with the GitLab CI.

## Use the package in another project

```sh
npm i -D @metabohub/LoadingMask
```

If your project is not using vuetify, you need to import it in `src/main.ts` :
```ts
import { createApp } from 'vue'
import App from './App.vue'
import { vuetify } from "@metabohub/LoadingMask";

createApp(App).use(vuetify).mount('#app')
```

Use the component : 
```ts
<script setup lang="ts">
import { ComponentExample } from "@metabohub/LoadingMask";
import "@metabohub/LoadingMask/dist/style.css";
</script>

<template>
  <ComponentExample />
</template>
```

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Type-Check, Compile and Minify for Production

```sh
npm run build
```

### Run Unit Tests with [Vitest](https://vitest.dev/)

```sh
npm run test:unit
```

Check the coverage :
```sh
npm run coverage
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```

### View stories with [Storybook](https://storybook.js.org/docs/vue/get-started/whats-a-story)

```sh
npm run storybook
```
