import type { Meta, StoryObj } from "@storybook/vue3";
import LoadingMask from "../components/LoadingMask.vue";

import { VApp } from "vuetify/lib/components/index.mjs";

const meta: Meta<typeof LoadingMask> = {
	title: "LoadinMask component",
	component: LoadingMask,
	tags: ["autodocs"],
};

export default meta;
type Story = StoryObj<typeof LoadingMask>;

export const Default: Story = {
	render: () => ({
		components: { LoadingMask, VApp },
		template: `<v-app>
	<h1>Default props</h1>
	<LoadingMask /></v-app>`,
	}),
};

export const ChangeStyle: Story = {
	render: () => ({
		components: { LoadingMask, VApp },
		template: `<v-app>
	<h1>Change background, font, progress bar color, font size, or background opacity</h1>
	<LoadingMask bg-color="green" font-color="white"
  progress-color="yellow"
  :bg-opacity="0.6"
  :font-size="12"/></v-app>`,
	}),
};

export const MakeProgressBarCircular: Story = {
	render: () => ({
		components: { LoadingMask, VApp },
		template: `<v-app>
	<h1>Make the progress bar circular</h1>
	<LoadingMask progress-style="circular" /></v-app>`,
	}),
};


export const ChangeText: Story = {
	render: () => ({
		components: { LoadingMask, VApp },
		template: `<v-app>
	<h1>Use a slot to put anything else at the place of the text</h1>
	<LoadingMask :font-size="12">
		<template v-slot:loading-text>
			<h2>This is loading...</h2>
			<h3>Please be patient.................</h3>
		</template>
	</LoadingMask>
	</v-app>`
	}),
};

export const ChangeImageSlot: Story = {
	render: () => ({
		components: { LoadingMask, VApp },
		template: `<v-app>
	<h1>Use a slot to replace the progress bar by anthything else</h1>
	<LoadingMask>
	<template #loading-image>
		<svg xmlns="http://www.w3.org/2000/svg">
			<circle fill="#FF156D" stroke="#FF156D" stroke-width="15" r="15" cx="35"
				cy="50">
				<animate attributeName="cx" calcMode="spline" dur="2"
					values="35;165;165;35;35"
					keySplines="0 .1 .5 1;0 .1 .5 1;0 .1 .5 1;0 .1 .5 1"
					repeatCount="indefinite" begin="0"></animate>
			</circle>
			<circle fill="#FF156D" stroke="#FF156D" stroke-width="15" opacity=".8" r="15"
				cx="35" cy="50">
				<animate attributeName="cx" calcMode="spline" dur="2"
					values="35;165;165;35;35"
					keySplines="0 .1 .5 1;0 .1 .5 1;0 .1 .5 1;0 .1 .5 1"
					repeatCount="indefinite" begin="0.05"></animate>
			</circle>
			<circle fill="#FF156D" stroke="#FF156D" stroke-width="15" opacity=".6" r="15"
				cx="35" cy="50">
				<animate attributeName="cx" calcMode="spline" dur="2"
					values="35;165;165;35;35"
					keySplines="0 .1 .5 1;0 .1 .5 1;0 .1 .5 1;0 .1 .5 1"
					repeatCount="indefinite" begin=".1"></animate>
			</circle>
			<circle fill="#FF156D" stroke="#FF156D" stroke-width="15" opacity=".4" r="15"
				cx="35" cy="50">
				<animate attributeName="cx" calcMode="spline" dur="2"
					values="35;165;165;35;35"
					keySplines="0 .1 .5 1;0 .1 .5 1;0 .1 .5 1;0 .1 .5 1"
					repeatCount="indefinite" begin=".15"></animate>
			</circle>
			<circle fill="#FF156D" stroke="#FF156D" stroke-width="15" opacity=".2" r="15"
				cx="35" cy="50">
				<animate attributeName="cx" calcMode="spline" dur="2"
					values="35;165;165;35;35"
					keySplines="0 .1 .5 1;0 .1 .5 1;0 .1 .5 1;0 .1 .5 1"
					repeatCount="indefinite" begin=".2"></animate>
			</circle>
		</svg>
	</template>
  </LoadingMask></v-app>`
	})
};


